import xss from "xss";
import { Message } from 'element-ui';
/**
 * 过滤输入字符串，防范XSS
 * @param {String} str 
 */
export const interceptXSS = str => {
    let value = '';
    if (str) {
        value = xss(str, {
            whiteList: [], // 白名单为空，表示过滤所有标签
            stripIgnoreTag: true, // 过滤所有非白名单标签的HTML
            stripIgnoreTagBody: ["script"] // script标签较特殊，需要过滤标签中间的内容
        });
    }
    return value;
}

/**
 * 存储localStorage
 */
export const setStore = (name, content) => {
    if (!name) return;
    if (typeof content !== 'string') {
        content = JSON.stringify(content);
    }
    window.localStorage.setItem(name, content);
}

/**
 * 获取localStorage
 */
export const getStore = name => {
    if (!name) return;
    var value = window.localStorage.getItem(name);
    if (value !== null) {
        try {
            value = JSON.parse(value);
        } catch (e) {
            value = value;
        }
    }
    return value;
}

/**
 * 删除localStorage
 */
export const removeStore = name => {
    if (!name) return;
    window.localStorage.removeItem(name);
}

/**
 * 存储SessionStorage
 */
export const setSessionStore = (name, content) => {
    if (!name) return;
    if (typeof content !== 'string') {
        content = JSON.stringify(content);
    }
    window.sessionStorage.setItem(name, content);
}

/**
 * 获取SessionStorage
 */
export const getSessionStore = name => {
    if (!name) return;
    var value = window.sessionStorage.getItem(name);
    if (value !== null) {
        try {
            value = JSON.parse(value);
        } catch (e) {
            value = value;
        }
    }
    return value;
}

/**
 * 删除SessionStorage
 */
export const removeSessionStore = name => {
    if (!name) return;
    window.sessionStorage.removeItem(name);
}

/**
 * 让整数自动保留2位小数
 */
// export const returnFloat = value => { 
//     var value=Math.round(parseFloat(value)*100)/100; 
//     var xsd=value.toString().split("."); 
//     if(xsd.length==1){ 
//         value=value.toString()+".00"; 
//         return value;   
//     } 
//     if(xsd.length>1){ 
//         if(xsd[1].length<2){ 
//             value=value.toString()+"0"; 
//         } 
//         return value; 
//     } 
// } 
/**
 * @param {date} 标准时间格式:Fri Nov 17 2017 09:26:23 GMT+0800 (中国标准时间)
 * @param {type} 类型
 *   type == 1 ---> "yyyy-mm-dd hh:MM:ss.fff"
 *   type == 2 ---> "yyyymmddhhMMss"
 *   type == 3 ---> "yyyymmdd"
 *   type == '' ---> "yyyy-mm-dd hh:MM:ss"
 */
export const formatDate = (date, type) => {
    var year = date.getFullYear(); //年
    var month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1; //月
    var day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate(); //日
    var hour = date.getHours() < 10 ? "0" + date.getHours() : date.getHours(); //时
    var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes(); //分
    var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds(); //秒
    var milliseconds = date.getMilliseconds() < 10 ? "0" + date.getMilliseconds() : date.getMilliseconds() //毫秒
    if (type == 1) {
        return year + "-" + month + "-" + day + " " + hour + ":" + minutes + ":" + seconds + "." + milliseconds;
    } else if (type == 2) {
        return year + "" + month + "" + day + "" + hour + "" + minutes + "" + seconds;
    } else if (type == 3) {
        return year + "-" + month + "-" + day;
    } else {
        return year + "-" + month + "-" + day + " " + hour + ":" + minutes + ":" + seconds;
    }
}
/**
 * 时间转换：20150101010101 --> '2015-01-01 01:01:01'
 */
export const parseToDate = (timeValue) => {
    var result = "";
    var year = timeValue.substr(0, 4);
    var month = timeValue.substr(4, 2);
    var date = timeValue.substr(6, 2);
    var hour = timeValue.substr(8, 2);
    var minute = timeValue.substr(10, 2);
    var second = timeValue.substr(12, 2);
    result = year + "-" + month + "-" + date + " " + hour + ":" + minute + ":" + second;
    return result;
}
/**
 * 判断空值
 */
export const isEmpty = (keys) => {
    if (typeof keys === "string") {
        keys = keys.replace(/\"|&nbsp;|\\/g, '').replace(/(^\s*)|(\s*$)/g, "");
        if (keys == "" || keys == null || keys == "null" || keys === "undefined") {
            return true;
        } else {
            return false;
        }
    } else if (typeof keys === "undefined") { // 未定义
        return true;
    } else if (typeof keys === "number") {
        return false;
    } else if (typeof keys === "boolean") {
        return false;
    } else if (typeof keys == "object") {
        if (JSON.stringify(keys) == "{}") {
            return true;
        } else if (keys == null) { // null
            return true;
        } else {
            return false;
        }
    }

    if (keys instanceof Array && keys.length == 0) { // 数组
        return true;
    }

}

/**
 * 返回两位的小数的字符串
 */
export const toFixedNum = (num) => {
    const tonum = Number(num).toFixed(2);
    return tonum;
}

export const showMessage = () => {
    this.$message({
        showClose: true,
        message: '对不起，您暂无此操作权限~',
        type: 'success'
    });
}

/**
 * 读取base64
 */
export const readFile = file => {
    console.log(file)
    //var file = this.files[0];
    //判断是否是图片类型
    if (!/image\/\w+/.test(file.raw.type)) {
        alert("只能选择图片");
        return false;
    }
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function (e) {
        var filedata = {
            filename: file.name,
            filebase64: e.target.result
        }
        alert(e.target.result)
    }
}

/**
 * 动态插入css
 */
export const loadStyle = url => {
    const link = document.createElement('link')
    link.type = 'text/css'
    link.rel = 'stylesheet'
    link.href = url
    const head = document.getElementsByTagName('head')[0]
    head.appendChild(link)
}

/**
 * 设置浏览器头部标题
 */
export const setTitle = (title) => {
    title = title ? `${title}` : 'Admin'
    window.document.title = title
}

export const param2Obj = url => {
    const search = url.split('?')[1]
    if (!search) {
        return {}
    }
    return JSON.parse('{"' + decodeURIComponent(search).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g, '":"') + '"}')
}

//是否为正整数
export const isInteger = (s) => {
    var re = /^[0-9]+$/;
    return re.test(s)
}
// 设置容器高度
export const setContentHeight = (that, ele, height) => {
    that.$nextTick(() => {
        ele.style.height = (document.body.clientHeight - height) + 'px'
    })
}


/** 图片压缩，默认同比例压缩
 *  @param {Object} fileObj
 *  图片对象
 *  回调函数有一个参数，base64的字符串数据
 */
export const compress = (fileObj, callback) => {
    try {
        const image = new Image()
        image.src = URL.createObjectURL(fileObj)
        image.onload = function () {
            const that = this
            // 默认按比例压缩
            let w = that.width
            let h = that.height
            const scale = w / h
            w = fileObj.width || w
            h = fileObj.height || (w / scale)
            let quality = 0.7 // 默认图片质量为0.7
            // 生成canvas
            const canvas = document.createElement('canvas')
            // 返回一个用于在画布上绘图的环境
            const ctx = canvas.getContext('2d')
            // 创建属性节点，指定名称的属性，并返回Attr 对象属性
            const anw = document.createAttribute('width')
            anw.nodeValue = w
            const anh = document.createAttribute('height')
            anh.nodeValue = h
            // 给元素添加新的属性节点
            canvas.setAttributeNode(anw)
            canvas.setAttributeNode(anh)
            // 在画布上绘制图像、画布或视频。也能够绘制图像的某些部分，以及/或者增加或减少图像的尺寸。
            ctx.drawImage(that, 0, 0, w, h)
            // 图像质量
            if (fileObj.quality && fileObj.quality <= 1 && fileObj.quality > 0) {
                quality = fileObj.quality
            }
            // 返回一个包含图片展示的 data URI，在指定图片格式为 image/jpeg 或 image/webp的情况下quality值越小，所绘制出的图像越模糊
            const data = canvas.toDataURL('image/jpeg', quality)
            // 压缩完成执行回调
            const newFile = convertBase64UrlToBlob(data)
            callback(newFile)
        }
    } catch (e) {
        console.log('图片压缩失败!')
    }
}
// 压缩后，canvas的toBlob方法，把当前绘制信息转为一个Blob对象抛出
function convertBase64UrlToBlob(urlData) {
    const bytes = window.atob(urlData.split(',')[1]) // 去掉url的头，并转换为byte
    // 处理异常,将ascii码小于0的转换为大于0
    const ab = new ArrayBuffer(bytes.length)
    const ia = new Uint8Array(ab)
    for (let i = 0; i < bytes.length; i++) {
        ia[i] = bytes.charCodeAt(i)
    }
    return new Blob([ab], { type: 'image/png' })
}



/**
 * 格式化objact为formdata
 * 数据存在问题，待修正
 */
// export const toFormData2 = (obj) => {
//     let formData = new FormData();
//     for (const key in obj) {
//         if (obj[key]) {
//             formData.append(encodeURIComponent(key), encodeURIComponent(obj[key]));
//         }
//     }
//     console.log(formData);
//     return formData;
// }

/**
 * 格式化objact为formdata
 */
export const toFormData = (obj) => {
    let ret = "";
    for (let i in obj) {
        ret += encodeURIComponent(i) + "=" + encodeURIComponent(obj[i]) + "&";
    }
    if (ret.length > 0) {
        ret = ret.substring(0, ret.length - 1);
    }
    return ret;
}



/**
 * jpg/jpeg/png 图片格式校验
 * @param {String} imgType 
 */
export const checkImageType = (imgType) => {
    const regx = new RegExp("(image/jpg)$|(image/jpeg)$|(image/png)$");
    if (!regx.test(imgType)) {
        Message.error("Drop your png/jpg/jpeg files here!");
        return false;
    } else {
        return true;
    }
}

/**
 * jpg/jpeg/png 图片大小校验
 * @param {Number} imgSize 
 * @param {Number} maxSize 
 */
export const checkImageSize = (imgSize, maxSize) => {
    if (imgSize / (1024 * 1024) > maxSize) {
        Message.error("Max size " + maxSize + "M!");
        return false
    } else {
        return true
    }
}

/**
 * 获取上传路径
 * 0-资质，1-营业执照，2-税务登记证
 * @param {Number} num 
 */
export const getUploadPath = (num) => {
    let path = "";
    if (num) {
        switch (num) {
            case 0:
                path = "../资质/";
                break;
            case 1:
                path = "../营业执照/";
                break;
            case 2:
                path = "../税务登记证/";
                break;
            // case 3:
            //      path= "../资质/";
            //     break;
            // case 4:
            //      path= "../资质/";
            //     break;
            default:
                break;
        }
    } else {
        console.log("getUploadPath -> num -> null");
    }
    return path;
}

/**
 * 对象内key转化为小写
 * @param {Object} data
 */
export const transformationObjectKey = (data) => {
    let obj = JSON.parse(JSON.stringify(data));
    for (const key in obj) {
        obj[key.toString().toLocaleLowerCase()] = obj[key];
        delete obj[key];
    }
    return obj;
}
