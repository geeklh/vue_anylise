import axios from "axios";
import { ElTabPane } from "element-ui/types/tab-pane";

export const downLoad=(api, fileName,fileType) =>{
    axios({
        method: 'get',
        url: process.env.BASE_API + api,
        responseType: 'blob',
        contentType: "application/x-www-form-urlencoded"
    }).then(res => {
        let data = res.data;
        // 根据文件类型匹配后缀等配置对象
        let filesuffixObj=Getfilesuffix(fileType);
        if (JSON.stringify(filesuffixObj) != "{}") {
            // Blob数据生成下载url
            let href = window.URL.createObjectURL(new Blob([data], { type: filesuffixObj.contentType}));
            // 创建下载元素
            let link = document.createElement('a');
            link.style.display = 'none';
            link.href = href;
            link.setAttribute('download', fileName + filesuffixObj.suffix);
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link); // 移除下载元素
            window.URL.revokeObjectURL(href); //释放掉blob对象 
        }
    }).catch((error) => {
        console.log(error);
    })
}

// 文件后缀名数组
const fileTypeObj=[
    {
        type:"文档",
        suffix:".docx",
        contentType:'application/vnd.openxmlformats-officedocument.wordprocessingml.document;charset=utf-8'
    },
    {
        type:"表格",
        suffix:".xlsx",
        contentType:'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8'
    },
    {
        type:"图片",
        suffix:".png",
        contentType:'image/png;charset=utf-8'
    },
    {
        type:"文本文档",
        suffix:".txt",
        contentType:'text/plain;charset=utf-8'
    },
    {
        type:"压缩文件",
        suffix:".rar",
        contentType:'application/octet-stream;charset=utf-8'
    },
    {
        type:"便携式文档",
        suffix:".pdf",
        contentType:'application/pdf;charset=utf-8'
    }
];
/**
 * 获取当前文件类型对应的文件后缀名和类型
 * @param {String} fileType 
 */
function Getfilesuffix(fileType){
    let obj={};
    if (fileType) {
        fileTypeObj.forEach(item=>{
            if (item.type==fileType) {
                obj=item;
            }
        })
    }
    return obj;
}