
const role = [
    {
        type: '系统管理员',
        value: ['superAdmin']
    },
    {
        type: '管理员',
        value: ['admin']
    },
    {
        type: '监督机构',
        value: ['supervisoryOrganization']
    },
    {
        type: '监测员',
        value: ['monitor']
    },
    {
        type: '监督员',
        value: ['supervisor']
    }
]
//  获取对应角色字段
export function getRoles(key) {
    let value = [];
    role.find(item => {
        if (item.type == key) {
            value = item.value
        }
    })
    return value
}

// 项目管理Tab项
const editableTabs = [
    {
        // 机构信息
        title: "unitInformationList",
        content: "UnitInformation",
        roles: ['admin']
    },
    {
        // 项目列表
        title: "projectList",
        content: "ProjectList",
        roles: ['admin', "monitor", 'supervisor']
    },
    {
        // 监测员管理
        title: "monitorList",
        content: "Monitor",
        roles: ['admin']
    },
    {
        // 监督员管理
        title: "supervisionList",
        content: "Supervision",
        roles: ['monitor']
    },
    // {
    //     // 监督机构管理
    //     title: "supervisionList2",
    //     content: "Supervision",
    //     roles: ['管理员']
    // },
    {
        // 消警记录
        title: "WarningRecord",
        content: "WarningRecord",
        roles: ['admin']
    },
    {
        //仪器管理
        title: "Equipment",
        content: "Equipment",
        roles: ['admin']
    }
];

/**
 * 根据当前用户角色过滤出有权限操作项目管理Tab项
 * @param {String} userType 
 */
export function filterTabs(userType) {
    const tab = editableTabs.filter(item => {
        return (item.roles.some(value => {
            return (value.indexOf(userType) > -1)
        }));
    })
    for (let index = 0; index < tab.length; index++) {
        tab[index].name = (index + 1).toString()
    }
    return tab
}

