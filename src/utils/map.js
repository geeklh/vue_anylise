import "leaflet/dist/leaflet.css"
import $L from "leaflet";
// 解决 leaflet 默认 maker 无法显示的问题
import icon from "leaflet/dist/images/marker-icon.png";
import iconShadow from "leaflet/dist/images/marker-shadow.png";
let DefaultIcon = $L.icon({
    iconUrl: icon,
    shadowUrl: iconShadow
});
$L.Marker.prototype.options.icon = DefaultIcon;

/**
 * 创建地图容器
 * @param {String} divId 
 * @param {Object} options
 */
const createMap = (divId, options) => {
    let map = $L.map(divId, options);
    return map;
};
/**
 * 图层加载
 * @param {Object} map
 * @param {String} url 
 * @param {Object'} options
 */
const createTileLayer = async (map, url, options) => {
    let tileLayer = await $L.tileLayer(url, options);
    tileLayer.addTo(map);
    return tileLayer
}
/**
 * 创建标记点
 * @param {Object} options
 */
const createIcon = options => {
    return $L.icon(options);
}

/**
 * 通过 [x, y] 坐标添加 Maker
 *
 * @param {Object} map
 * @param {Array} coordinate
 * @param {Object} options
 * 
 */
const createMakerByXY = (map, coordinate, options = {}) => {
    let marker = $L.marker($L.latLng(coordinate[0], coordinate[1]), options);
    marker.addTo(map);
    return marker;
};

export { createMap, createTileLayer, createIcon, createMakerByXY };

