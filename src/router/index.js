import Vue from 'vue'
import Router from 'vue-router'
// import { listhome } from "../layoutlist"//新加布局
import { homeLayout } from "../layoutlist/loindexlayout"//新加两段式首页布局
import { Layout, Content } from "../layoutlist/layout"; // 页面整体布局
import { topRouterMap } from "./topRouter";

process.env.NODE_ENV === "development" ? Vue.use(Router) : null;

function filterTopRouterMap(name) {
    let router = topRouterMap.find((item) => {
        return item.parentName === name;
    });
    return router.data; // arr
}

/**
 * 1、roles:后台返回的权限结构;
 * 
 */
//手动跳转的页面白名单
const whiteList = [
    '/'
];
/**
 * path:''与path:'*'的区别：
 * 1、path:'*', 会匹配所有路径
 * 2、path:''，也是会匹配到路由
 * 
 */
//默认不需要权限的页面
export const constantRouterMap = [
    // {
    //     path: '',
    //     component: Layout,
    //     redirect: '/index/index',
    //     hidden: true
    // },
    {
        path: '',
        component: Layout,
        redirect: '/loindex/loindex',
        hidden: true
    },
    {
        path: '/login',
        name: 'login',
        component: () =>
            import('@/page/login'),
        hidden: true
    },
    {
        path: '/404',
        component: () =>
            import('@/page/errorPage/404'),
        hidden: true
    },
    {
        path: '/401',
        component: () =>
            import('@/page/errorPage/401'),
        hidden: true
    },
    {
        path: '/index',
        name: 'index',
        component: Layout,
        meta: {
            title: '首页',
            icon: 'icondashboard',
        },
        noDropdown: true,
        children: [{
            path: 'index',
            meta: {
                title: '首页',
                icon: 'icondashboard',
                routerType: 'leftmenu'
            },
            component: () =>
                import('@/page/index/index'),
        }]
    }
]

//注册路由
export default new Router({
    mode: 'history', // 默认为'hash'模式,去掉地址栏的#，没有就直接404
    base: '/permission/', // 添加跟目录,对应服务器部署子目录
    routes: constantRouterMap
})
// 新加的顶部路由
export const newTopMenu = [
    {
        path: '/loindex',
        name: 'loindex',
        component: homeLayout,
        meta: {
            title: '主页',
            icon: 'icondashboard',
            roles: ['admin', 'monitor', 'supervisor', 'supervisoryOrganization', 'editor']
        },
        noDropdown: true,
        children: [{
            path: 'loindex',
            meta: {
                title: '主页',
                icon: 'icondashboard',
                roles: ['admin', 'monitor', 'supervisor', 'supervisoryOrganization', 'editor'],
                routerType: 'topmenu'
            },
            component: () =>
                import('@/page/index/loindex')
        }]
    },
    {
        path: '/index',
        name: 'projectManage',
        component: homeLayout,
        meta: {
            title: '项目管理',
            icon: 'icondashboard',
            roles: ['admin', 'monitor', 'supervisor', 'supervisoryOrganization'],

        },
        noDropdown: true,
        children: [{
            path: 'projectManage',
            meta: {
                title: '项目管理',
                icon: 'icondashboard',
                routerType: 'topmenu'
            },
            component: () =>
                import('@/page/ProjectManage/projectManage'),
        }]
    },
    {
        path: '/fileManage',
        name: 'fileManage',
        component: homeLayout,
        meta: {
            title: '文件管理',
            icon: 'icondashboard',
            roles: ['admin', 'monitor', 'supervisor', 'supervisoryOrganization'],

        },
        noDropdown: true,
        children: [{
            path: 'fileManage',
            meta: {
                title: '文件管理',
                icon: 'icondashboard',
                routerType: 'topmenu'
            },
            component: () =>
                import('@/page/fileManage/fileManage'),
        }]
    },
    {
        path: '/curveGenerator',
        name: 'curveGenerator',
        component: homeLayout,
        meta: {
            title: '曲线配置',
            icon: 'iconecharts',
            roles: ['admin', 'monitor', 'supervisor', 'supervisoryOrganization'],
        },
        noDropdown: true,
        children: [{
            path: 'curveGenerator',
            meta: {
                title: '曲线配置器',
                icon: 'iconecharts',
                roles: ['admin', 'monitor', 'supervisor', 'supervisoryOrganization'],
                routerType: 'topmenu'
            },
            component: () =>
                import('@/page/curveGenerator/CurveGenerator'),
        }]
    },
    {
        path: '/institution',
        name: 'institution',
        component: homeLayout,
        meta: {
            title: '机构',
            icon: 'iconecharts',
            roles: ['superAdmin'],
        },
        noDropdown: true,
        children: [{
            path: 'institutionManage',
            meta: {
                title: '机构管理',
                icon: 'iconecharts',
                roles: ['superAdmin'],
                routerType: 'topmenu'
            },
            component: () =>
                import('@/page/institution/institution'),
        }]
    }
]

//异步路由（需要权限的页面）
export const asyncRouterMap = [
    {
        path: '/infoManage',
        name: 'infoManage',
        meta: {
            title: '信息管理',
            icon: 'iconinfo',
        },
        component: Layout,
        children: [
            {
                path: 'projectInformation',
                name: 'projectInformation',
                meta: {
                    title: '项目信息',
                    routerType: 'leftmenu'
                },
                component: () =>
                    import('@/page/infoManage/projectInformation/projectInformation'),
            },
            {
                path: 'monitoringProgram',
                name: 'monitoringProgram',
                meta: {
                    title: '监测方案',
                    routerType: 'leftmenu'
                },
                component: () =>
                    import('@/page/infoManage/monitoringProgram/monitoringProgram'),
            },
            {
                path: 'measuringPointManage',
                name: 'measuringPointManage',
                meta: {
                    title: '测点设置',
                    routerType: 'leftmenu'
                },
                component: () =>
                    import('@/page/infoManage/measuringPointManage/measuringPointManage'),
            },
            {
                path: 'floorPlan',
                name: 'floorPlan',
                meta: {
                    title: '平面图设置',
                    routerType: 'leftmenu'
                },
                component: () =>
                    import('@/page/infoManage/floorPlan/planeFigureForMarker'),
            }
        ]
    },
    {
        path: '/share',
        name: 'share',
        component: Layout,
        meta: {
            title: '数据录入',
            icon: 'iconshare',
            roles: ['admin']
        },
        noDropdown: true,
        children: [{
            path: 'share',
            meta: {
                title: '数据录入',
                icon: 'iconshare',
                routerType: 'leftmenu'
            },
            component: () =>
                import('@/page/share'),
        }]
    },
    {
        path: '/fundData',
        name: 'fundData',
        meta: {
            title: '数据查看',
            icon: 'iconecharts',
        },
        component: Layout,
        // redirect: '/fundData/fundPosition',
        children: [
            // {
            //     path: 'fundPosition',
            //     name: 'fundPosition',
            //     meta: {
            //         title: '投资分布',
            //         routerType: 'leftmenu'
            //     },
            //     component: () =>
            //         import('@/page/fundData/fundPosition')
            // },
            // {
            //     path: 'typePosition',
            //     name: 'typePosition',
            //     meta: {
            //         title: '项目分布',
            //         routerType: 'leftmenu'
            //     },
            //     component: () =>
            //         import('@/page/fundData/typePosition')
            // },
            // {
            //     path: 'incomePayPosition',
            //     name: 'incomePayPosition',
            //     meta: {
            //         title: '收支统计',
            //         routerType: 'leftmenu'
            //     },
            //     component: () =>
            //         import('@/page/fundData/incomePayPosition')
            // },
            {
                path: 'curve',
                name: 'curve',
                meta: {
                    title: '曲线详情',
                    routerType: 'leftmenu',
                    childrenList: true
                },
                component: () =>
                    import('@/page/fundData'),
                redirect: '/fundData/curve/curveDetails',
                children: [
                    {
                        path: 'curveDetails',
                        name: 'curveDetails',
                        meta: {
                            title: '曲线详情',
                            routerType: 'leftmenu'
                        },
                        component: () =>
                            import('@/page/fundData/components/curveDetails'),
                    },
                    {
                        path: 'table',
                        name: 'table',
                        meta: {
                            title: '表格',
                            routerType: 'leftmenu'
                        },
                        component: () =>
                            import('@/page/fundData/components/curveComponent'),
                    }
                ]
            }
        ]
    },
    {
        path: '/fundManage',
        name: 'fundManage',
        meta: {
            title: '报表管理',
            icon: 'iconpay3',
            roles: ['admin']
        },
        component: Layout,
        children: [{
            path: 'fundList',
            name: 'fundList',
            meta: {
                title: '资金流水',
                routerType: 'leftmenu'
            },
            component: () =>
                import('@/page/fundList/fundList'),
        },
        {
            path: 'chinaTabsList',
            name: 'chinaTabsList',
            meta: {
                title: '区域投资',
                routerType: 'leftmenu'
            },
            component: () =>
                import('@/page/fundList/chinaTabsList'),
        }
        ]
    },
    {
        path: '/resultManage',
        name: 'resultManage',
        component: Layout,
        meta: {
            title: '成果管理',
            icon: 'iconuser',
            roles: ['admin']
        },
        noDropdown: true,
        children: [{
            path: 'userList',
            meta: {
                title: '成果管理',
                icon: 'iconuser',
                routerType: 'leftmenu'
            },
            component: () =>
                import('@/page/userList/userList'),
        }]
    },
    {
        path: '/warningManage',
        name: 'warningManage',
        component: Layout,
        meta: {
            title: '预警管理',
            icon: 'iconuser',
        },
        children: [
            {
                path: 'warningProcessing',
                name: 'warningProcessing',
                meta: {
                    title: '预警处理',
                    icon: 'iconuser',
                    roles: ['admin', 'monitor'],
                    routerType: 'leftmenu'
                },
                component: () =>
                    import('@/page/warningManage/warningProcessing/warningProcessing'),
            },
            {
                path: 'SMSManage',
                name: 'SMSManage',
                meta: {
                    title: '短信记录',
                    icon: 'iconuser',
                    roles: ['admin', 'monitor'],
                    routerType: 'leftmenu'
                },
                component: () =>
                    import('@/page/warningManage/SMSManage/SMSManage'),
            }
        ]
    },
    {
        path: '/equipmentManage',
        name: 'equipmentManage',
        component: Layout,
        meta: {
            title: '仪器管理',
            icon: 'iconuser',
            roles: ['admin']
        },
        noDropdown: true,
        children: [{
            path: 'equipmentList',
            meta: {
                title: '仪器管理',
                icon: 'iconuser',
                routerType: 'leftmenu'
            },
            component: () =>
                import('@/page/equipmentManage/equipmentManage'),
        }]
    },
    {

        path: '/permission',
        name: 'permission',
        meta: {
            title: '权限设置',
            icon: 'iconpermission',
            roles: ['admin'] // you can set roles in root nav
        },
        component: Layout,
        redirect: '/permission/page',
        children: [{
            path: 'page',
            name: 'pagePer',
            meta: {
                title: '页面权限',
                routerType: 'leftmenu',
                roles: ['admin'] // or you can only set roles in sub nav
            },
            component: () =>
                import('@/page/permission/page'),
        }, {
            path: 'directive',
            name: 'directivePer',
            meta: {
                title: '按钮权限',
                routerType: 'leftmenu',
                roles: ['editor']
            },
            component: () =>
                import('@/page/permission/directive'),
        }]
    },
    // {
    //     path: '/error',
    //     component: Layout,
    //     name: 'errorPage',
    //     meta: {
    //         title: '错误页面',
    //         icon: 'iconError'
    //     },
    //     children: [{
    //         path: '401',
    //         name: 'page401',
    //         component: () =>
    //             import('@/page/errorPage/401'),
    //         meta: {
    //             title: '401',
    //             noCache: true
    //         }
    //     },
    //     {
    //         path: '404',
    //         name: 'page404',
    //         component: () =>
    //             import('@/page/errorPage/404'),
    //         meta: {
    //             title: '404',
    //             noCache: true
    //         }
    //     }
    //     ]
    // },
    { path: '*', redirect: '/404', hidden: true }
];

/**
 *  路由设置：
 * 1、该路由有子菜单,可以设置多层嵌套路由children;如果没有子菜单,不需要设置children;通过item.children.length来判断路由的级数;
 * 2、登录成功后,定位到系统首页时,需要加载页面整体布局组件并进行子路由定向加载;
 */