import bread from '../bread.vue'
import Layout from './home.vue'
import Content from '../content.vue'

export {
    Layout,
    Content,
    bread,
}
