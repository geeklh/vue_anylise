import request from '@/utils/axios'
const API = "/apis/author/Supervisor.ashx?type="
// 监督员项目管理模块

// 监督员项目列表加载
export function getProjectListFormSupervisor(data) {
    return request({
        url: API + 'xmload',
        method: 'POST', //请求方式
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        data: data
    })
}
