import request from '@/utils/axios'
const API = "/apis/author/monitor.ashx?type="
// 监测员项目管理模块

// 监测员添加新项目数据
export function addProject(userid, data) {
    return request({
        url: API + 'xmadd' + "&userid=" + userid,
        method: 'POST', //请求方式
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        data: data
    })
}

// 监测员项目列表加载
export function getProjectListFormMonitor(data) {
    return request({
        url: API + 'xmload',
        method: 'POST', //请求方式
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        data: data
    })
}

// 编辑项目信息
export function editProjectData(userId, data) {
    return request({
        url: API + 'xmupdate' + "&userid=" + userId,
        method: 'POST', //请求方式
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        data: data
    })
}

// 监督员列表加载
export function getSupervisorList(data) {
    return request({
        url: API + 'supervisorload',
        method: 'POST', //请求方式
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        data: data
    })
}

// 添加新监督员
export function addSupervisor(userId, data) {
    return request({
        url: API + 'supervisoradd' + "&userid=" + userId,
        method: 'POST', //请求方式
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        data: data
    })
}
// 更新监督员信息
export function editSupervisor(userId, data) {
    return request({
        url: API + 'supervisorupdate' + "&userid=" + userId,
        method: 'POST', //请求方式
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        data: data
    })
}

// 获取项目人员信息
export function getProjectUserData(data) {
    return request({
        url: API + 'unitxmmemberload',
        method: 'POST', //请求方式
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        data: data
    })
}