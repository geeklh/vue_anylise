import axios from 'axios'

// 通过经纬度获取结构化的地址信息
export function getGeocoder(params) {
    return new Promise((resolve, reject) => {
        axios.get('http://api.tianditu.gov.cn/geocoder', {
            params: params
        }).then(res => {
            resolve(res.data);
        }).catch(err => {
            reject(err.data)
        })


    })
}

// 通过地址信息获取结构化的经纬度信息
export function getLatlng(params) {
    return new Promise((resolve, reject) => {
        axios.get('http://api.tianditu.gov.cn/geocoder', {
            params: params
        }).then(res => {
            resolve(res.data);
        }).catch(err => {
            reject(err.data)
        })


    })
}


