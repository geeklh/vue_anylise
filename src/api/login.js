import request from '@/utils/axios'
const API = "/apis/author/pub.ashx?type="

// 人员登陆
export function login(data) {
  return request({
    url: API + 'login',
    method: 'POST', //请求方式
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    data: data
  })
}



