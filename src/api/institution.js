// 超级管理员项目管理模块
import request from '@/utils/axios'
const API = "/apis/author/superadministrator.ashx?type="

// 创建机构
export function createInstitution(data) {
  return request({
    url: API + 'unitAdd',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    data: data
  })
}

// 获取机构列表数据
export function getInstitutionList() {
  return request({
    url: API + 'unitload',
    method: 'GET',
    // params: params
  })
}

// 获取指定机构数据
export function queryInstitution(data) {
  return request({
    url: API + 'unitinfoload',
    method: 'POST', //请求方式
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    data: data
  })
}

// 编辑机构数据
export function editInstitution(data) {
  return request({
    url: API + 'unitEdit',
    method: 'POST', //请求方式
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    data: data
  })
}

// 获取管理人员列表数据
export function getAdminList(data) {
  return request({
    url: API + 'adminload',
    method: 'POST', //请求方式
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    data: data
  })
}

// 添加管理人员数据
export function addAdmin(data) {
  return request({
    url: API + 'adminadd',
    method: 'POST', //请求方式
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    data: data
  })
}

// 编辑管理人员数据
export function editAdmin(data) {
  return request({
    url: API + 'adminupdate',
    method: 'POST', //请求方式
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    data: data
  })
}

// 禁止机构登录
export function limitInstitutionLogin(data) {
  return request({
    url: API + 'loginaccessoff',
    method: 'POST', //请求方式
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    data: data
  })
}

// 允许机构登录
export function authorizeInstitutionLogin(data) {
  return request({
    url: API + 'loginaccesson',
    method: 'POST', //请求方式
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    data: data
  })
}




// // 获取所有配置数据
// export function getChartSetting(params) {
//   return request({
//     url: API + 'ChartSettingQuery',
//     method: 'get',
//     params: params
//   })
// }
// // 保存当前配置数据
// export function saveConfig(data) {
//   return request({
//     url: API + 'ChartSettingSave',
//     method: 'POST', //请求方式
//     headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
//     data: data
//   })
// }