import request from '@/utils/axios'
const API = "/apis/author/administrator.ashx?type="
// 管理员项目管理模块

// 管理员项目列表加载
export function getProjectListFormAdmin(data) {
    return request({
        url: API + 'xmload',
        method: 'POST', //请求方式
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        data: data
    })
}

// 获取监测员列表数据
export function getMonitorList(data) {
    return request({
        url: API + 'monitorload',
        method: 'POST', //请求方式
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        data: data
    })
}

// 添加管理人员数据
export function addMonitor(data) {
    return request({
        url: API + 'monitoradd',
        method: 'POST', //请求方式
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        data: data
    })
}
// 监测员信息编辑
export function editMonitor(data) {
    return request({
        url: API + 'monitorupdate',
        method: 'POST', //请求方式
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        data: data
    })
}