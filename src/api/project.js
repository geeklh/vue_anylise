import request from '@/utils/axios'
// 项目模块接口
const xmApi = "/apis/author/xm.ashx?type="

// 获取指定项目信息
export function getProjectData(data) {
    return request({
        url: xmApi + 'xminfoload',
        method: 'POST', //请求方式
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        data: data
    })
}



// 项目测点部分
const pointSetApi = "/apis/data/pointset.ashx?type="


//所有监测类型加载
export function getAllMonitorList(data) {
    return request({
        url: pointSetApi + 'sensortypeload',
        method: 'POST', //请求方式
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        data: data
    })
}

// 项目监测类型加载
export function getProjectMonitorList(data) {
    return request({
        url: pointSetApi + 'XmmonitorObjectLoad',
        method: 'POST', //请求方式
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        data: data
    })
}

// 项目监测类型添加
export function addProjectMonitorType(data) {
    return request({
        url: pointSetApi + 'XmmonitorObjectAdd',
        method: 'POST', //请求方式
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        data: data
    })
}


// 项目监测类型删除
export function deleteProjectMonitorType(data) {
    return request({
        url: pointSetApi + 'XmmonitorObjectDelete',
        method: 'POST', //请求方式
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        data: data
    })
}



// 传感器测点
const sensorApi = "/apis/data/sensor.ashx?type="

// 项目测点-传感器点名加载
export function getSensorPointList(userId, data) {
    return request({
        url: sensorApi + 'PointSetTableLoad' + "&userid=" + userId,
        method: 'POST', //请求方式
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        data: data
    })
}

// 项目测点-传感器测点添加
export function addSensorPoint(userId, data) {
    return request({
        url: sensorApi + 'PointSetAdd' + "&userid=" + userId,
        method: 'POST', //请求方式
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        data: data
    })
}

// 项目测点-传感器测点更新
export function updataSensorPoint(userId, data) {
    return request({
        url: sensorApi + 'PointSetUpdate' + "&userid=" + userId,
        method: 'POST', //请求方式
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        data: data
    })
}

// 项目测点-传感器测点删除
export function deleteSensorPoint(userId, data) {
    return request({
        url: sensorApi + 'PointSetDelete' + "&userid=" + userId,
        method: 'POST', //请求方式
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        data: data
    })
}

// 项目测点-测点批量设置预警
export function batchSettingWarnings(userId, data) {
    return request({
        url: sensorApi + 'MultiUpdate' + "&userid=" + userId,
        method: 'POST', //请求方式
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        data: data
    })
}






// 预警
const warningApi = "/apis/data/pointset.ashx?type="

// 测点预警列表加载
export function getAlarmValueList(data) {
    return request({
        url: warningApi + 'alarmvaluesetLoad',
        method: 'POST', //请求方式
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        data: data
    })
}

// 测点预警添加
export function addAlarmValue(data) {
    return request({
        url: warningApi + 'alarmvaluesetAdd',
        method: 'POST', //请求方式
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        data: data
    })
}

// 测点预警更新
export function updataAlarmValue(data) {
    return request({
        url: warningApi + 'alarmvaluesetUpdate',
        method: 'POST', //请求方式
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        data: data
    })
}

// 测点预警删除
export function deleteAlarmValue(data) {
    return request({
        url: warningApi + 'alarmvaluesetDelete',
        method: 'POST', //请求方式
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        data: data
    })
}

// 项目预警参数名称加载
export function getProjectWarningList(data) {
    return request({
        url: warningApi + 'xmalarmvaluesetNameLoad',
        method: 'POST', //请求方式
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        data: data
    })
}