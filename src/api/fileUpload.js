import request from '@/utils/axios'
const API = "/apis/author/pub.ashx?type="


// 上传图片
export function fileUpload(data) {
  return request({
    url: API + "fileupload",
    method: 'post',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    data: data
  })
}

// 删除图片
export function deleteFile(data) {
  return request({
    url: API + 'filedel',
    method: 'post',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    data: data
  })
}

// 获取某个指定的角色或机构的某种类型图片数据集



// // 获取所有配置数据
// export function getChartSetting(params) {
//   return request({
//     url: API + 'ChartSettingQuery',
//     method: 'get',
//     params: params
//   })
// }
