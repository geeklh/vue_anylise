import request from '@/utils/axios'
const API = "/apis/ChartSettingCreateServer.ashx?type="

// 获取监测类型数据
export function getSensortypeList(params) {
  return request({
    url: API + 'SensorTypeLoad',
    method: 'get',
    params: params
  })
}
// 获取数据字段下拉数据
export function getResultDataStruct(data) {
  return request({
    url: API + 'ResultDataStructLoad',
    method: 'post',
    // headers: {
    //   'Content-Type': 'application/x-www-form-urlencoded'
    // },
    data: data
  })
}
// 获取所有配置数据
export function getChartSetting(params) {
  return request({
    url: API + 'ChartSettingQuery',
    method: 'get',
    params: params
  })
}
// 保存当前配置数据
export function saveConfig(data) {
  return request({
    url: API + 'ChartSettingSave',
    method: 'POST', //请求方式
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    data: data
  })
}