
// 引入i18n国际化插件
import { getToken } from '@/utils/auth'
import Vue from 'vue'
import VueI18n from 'vue-i18n'
// element-ui
import locale from "element-ui/lib/locale";
import elementEn from 'element-ui/lib/locale/lang/en'; // element-ui 英语语言包
import elementZhCN from 'element-ui/lib/locale/lang/zh-CN'; // element-ui 中文语言包
import elementZhTW from 'element-ui/lib/locale/lang/zh-TW'; // element-ui 中文繁体语言包
//项目内
import enLocale from './en'
import zhLocale from './zh'
import zh_TWLocale from './zh_TW'

process.env.NODE_ENV === "development" ? Vue.use(VueI18n) : null;
// 注册i18n实例并引入解构语言文件
const i18n = new VueI18n({
  locale: getToken('lang') || 'zh',
  messages: {
    zh: {
      ...zhLocale,
      ...elementZhCN
    },
    en: {
      ...enLocale,
      ...elementEn
    },
    zh_TW: {
      ...zh_TWLocale,
      ...elementZhTW
    }
  }
});
// element-ui组件响应语言切换
locale.i18n((key, value) => i18n.t(key, value))
export default i18n;