import { TableColumn } from "element-ui";

const zh = {
  // 公共操作
  publicOperation: {
    operation: '操作',
    edit: '编辑',
    add: '添加',
    delete: '删除',
    updata: '上传',
    download: '下载',
    determine: '确定',
    cancel: '取消',
    save: '保存',
    details: '详情',
    warning: '预警',
    undone: "监测中",
    finish: '已完成',
    all: "全部",
    return: '返回'
  },
  commons: {
    xiaoai: '小爱',
    superAdmin: '超级管理员',
    admin: '管理员',
    editor: '赵晓编',
    quit: '退出',
    hi: '您好',
    index: '首页',
    userManage: '用户管理',
    share: '数据录入',
    infoManage: '信息管理',
    // 信息管理子菜单-
    projectInformation: '项目信息',
    monitoringProgram: '监测方案',
    measuringPointManage: '测点设置',
    floorPlan: '平面图设置',
    // -信息管理子菜单
    loindex: '主页',
    projectManage: '项目管理',
    fileManage: '文件管理',
    // 项目详情
    projectDetails: '项目详情',
    // 项目详情
    infoShow: '个人信息',
    infoShow1: '个人信息子菜单1',
    infoShow2: '个人信息子菜单2',
    infoShow3: '个人信息子菜单3',
    infoShow4: '个人信息子菜单4',
    infoShow5: '个人信息子菜单5',
    infoModify: '修改信息',
    infoModify1: '修改信息子菜单1',
    infoModify2: '修改信息子菜单2',
    infoModify3: '修改信息子菜单3',
    // 报表管理
    fundManage: '报表管理',
    fundList: '资金流水',
    chinaTabsList: '区域投资',
    // 数据查看
    fundData: '数据查看',
    fundPosition: '投资分布',
    typePosition: '项目分布',
    incomePayPosition: '收支分布',
    curve: '曲线',
    table: '表格',
    curveDetails: '曲线详情',
    // 成果管理
    resultManage: '成果管理',
    // 预警管理
    warningManage: '预警管理',
    warningProcessing: '预警处理',
    SMSManage: '短信记录',
    // 仪器管理
    equipmentManage: '仪器管理',

    permission: '权限设置',
    pagePer: '页面权限',
    directivePer: '按钮权限',
    errorPage: '错误页面',
    page401: '401',
    page404: '404',
    wechatNumber: '微信号',
    curveGenerator: '曲线配置',
    // 机构管理
    institution: '机构管理',
    institutionManage: '机构管理',
  },
  index: {
    yearLoss: '年度总盈亏',
    yearProfit: '年度收益率',
    potentialInvestor: '潜在投资人',
    intentionInvestor: '意向投资人',
    waitExamineInvestor: '待审投资人',
    examiningInvestor: '审核中投资人',
    tenMillion: '千万元',
    person: '人'
  },
  // 项目设置
  projectManage: {
    tabPane: {
      projectList: '项目列表',
      monitorList: '监测员管理',
      supervisionList: '监督员管理',
      unitInformationList: '机构信息管理',
      WarningRecord: '预警记录管理',
      Equipment: '仪器管理'
    },
    unitInformationList: {
      title: '机构信息管理',
      from: {
        unitno: '单位名称',
        address: '单位地址',
        region: '机构性质',
        contacts: '联系人',
        phone: '联系电话',
        email: '邮箱',
        selectedOptions: '所属区域',
        upload1: '选取资质文件',
        upload2: '选取营业执照',
        upload3: '选取税务登记证',
      }
    },
    projectList: {
      addProject: '新增项目',
      allProject: '全部项目',
      undoneProject: '监测中项目',
      finishProject: '已完成项目',
      table: {
        index: '序号',
        id: '项目编号',
        name: '项目名',
        address: '项目地址',
        dbName: '数据库名',
        monitoringUnit: '监测单位',
        requesterUnit: '委托单位',
        period: '监测周期',
        status: '监测状态',
        operation: '操作',
        startTime: '开始时间',
        endTime: '结束时间',
        securityLevel: '安全等级',
        principal: '负责人',
        projectType: '项目类型',
        monitor: '监测人员',
        supervisor: '监督人员',
        undone: '监测中',
        finish: '已完成',
        mobileStationInteval: '流动测站时间'
      }
    },
    monitorList: {
      title: '监测员列表',
      table: {
        name: '姓名',
        account: '账号',
        pass: '密码',
        position: '职务',
        tel: '电话',
        email: '邮箱',
        titleCertificate: '职称证书名称',
        titleCertificateID: '职称证书编号',
        inductionCertificate: '上岗证书名称',
        inductionCertificateID: '上岗证书编号',
        qualification: '资质'
      }
    },
    supervisionList: {
      title: '监督员列表',
      table: {
        name: '姓名',
        account: '账号',
        pass: '密码',
        tel: '电话',
        email: '邮箱',
        position: '职务',
        unitno: '归属机构',

      }
    },
    WarningRecord: {
      table: {
        name: '账户名',
        projectName: '项目名',
        sensorType: '监测类型',
        pointName: '点名',
        warningDetails: '预警详情',
        time: '时间',
        details: '详情',
      }
    },
    Equipment: {
      table: {
        senorno: '设备编号',
        verificationType: '检定预警',
        monitorName: '检定员',
        ProjectName: '当前项目',
        address: '地址',
        senorName: '设备名',
        senorType: '设备类型',
        lastInspectionTime: '最后检定时间',
        lastInspectionMan: '检定人',
        producetionTime: '生产时间',
        overdue: '过期',
        normal: '正常'
      }
    },
    // 项目测点
    // pointManage: {
    //   warning: {
    //     thisUpperLimit: '本次上限',
    //     thisLowerLimit: '本次下限',
    //     accruedUpperLimit: "累计上限",
    //     accruedLowerLimit: '累计下限'
    //   }
    // },
    // 预警参数字段
    warningKey: {
      name: '预警名称',
      ac_u: '累计上限',
      ac_l: '累计下限',
      ac: '累计',
      this: '本次',
      v_u: '值上限',
      v_l: '值下限',
      X_ac_u: 'X方向累计上限',
      X_ac_l: 'X方向累计下限',
      Y_ac_u: 'Y方向累计上限',
      Y_ac_l: 'Y方向累计下限',
      Z_ac_u: 'Z方向累计上限',
      X_this: 'X方向本次',
      Y_this: 'Y方向本次',
      Z_this: 'Z方向本次',
      rap: '变化速率',
      day_rap: '日变化',
    }
  }
}

export default zh;