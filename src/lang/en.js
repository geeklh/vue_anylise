const zh = {
  // layout
  publicOperation: {
    operation: 'Operation',
    edit: 'Edit',
    add: 'Add',
    delete: 'Delete',
    updata: 'Updata',
    download: 'Download',
    determine: 'Determine',
    cancel: 'Cancel',
    save: 'Save',
    details: 'Details',
    warning: 'Warning',
    undone: "Undone",
    finish: 'Finish',
    all: "All",
    return: 'Return'
  },
  commons: {
    xiaoai: 'Ai.',
    superAdmin: 'superAdmin',
    admin: 'Admin',
    editor: 'Editor',
    quit: 'Sign Out',
    hi: 'Hi',
    index: 'Dashboard',
    userManage: 'Users',
    share: 'Share',
    infoManage: 'Infos',
    // 信息管理子菜单-
    projectInformation: 'ProjectInfo',
    monitoringProgram: 'MonitorProgram',
    measuringPointManage: 'MeasuringPoint',
    floorPlan: 'FloorPlan',
    // -信息管理子菜单
    loindex: 'loindex',
    projectManage: 'ProjectManage',
    fileManage: 'fileManage',
    // 项目详情
    projectDetails: 'ProjectDetails',
    // 项目详情
    infoShow: 'InfoShow',
    infoShow1: 'InfoShow1',
    infoShow2: 'InfoShow2',
    infoShow3: 'InfoShow3',
    infoShow4: 'InfoShow4',
    infoShow5: 'InfoShow5',
    infoModify: 'InfoModify',
    infoModify1: 'InfoModify1',
    infoModify2: 'InfoModify2',
    infoModify3: 'InfoModify3',
    // 报表管理
    fundManage: 'Money',
    fundList: 'MoneyList',
    chinaTabsList: 'AreaList',
    // 数据查看
    fundData: 'FundData',
    fundPosition: 'FundPosition',
    typePosition: 'TypePosition',
    incomePayPosition: 'IncomePayPosition',
    curve: 'Curve',
    table: 'Table',
    curveDetails: 'CurveDetails',
    // 成果管理
    resultManage: 'ResultManage',
    // 预警管理
    warningManage: 'WarningManage',
    warningProcessing: 'WarningProcessing',
    SMSManage: 'SMSManage',
    // equipmentManage
    equipmentManage: 'EquipmentManage',

    permission: 'Permission',
    pagePer: 'PagePermission',
    directivePer: 'DirectivePermission',
    errorPage: 'ErrorPage',
    page401: '401',
    page404: '404',
    wechatNumber: 'wechat',
    curveGenerator: 'curveGenerator',
    // 机构管理
    institution: 'InstitutionManage',
    institutionManage: 'InstitutionManage',
  },
  index: {
    yearLoss: 'Year Loss',
    yearProfit: 'Year Profit',
    potentialInvestor: 'Potential Investor',
    intentionInvestor: 'Intention Investor',
    waitExamineInvestor: 'Wait Examine Investor',
    examiningInvestor: 'Examining Investor',
    tenMillion: 'Ten Million',
    person: 'P'
  },
  projectManage: {
    tabPane: {
      projectList: 'ProjectList',
      monitorList: 'MonitorList',
      supervisionList: 'SupervisionList',
      unitInformationList: 'UnitInformationList',
      WarningRecord: 'WarningRecord',
      Equipment: 'Equipment'
    },
    unitInformationList: {
      title: 'Unit Information Manage',
      from: {
        unitno: 'Unitno',
        address: 'Address',
        region: 'Region',
        cntacts: 'Cntacts',
        phone: 'Phone',
        email: 'Email',
        selectedOptions: 'Selectedoptions',
        upload1: 'Select Qualification Documents',
        upload2: 'Select Business License',
        upload3: 'Select Tax Registration Certificate',
      }
    },
    projectList: {
      addProject: 'New Project',
      allProject: 'All Project',
      undoneProject: 'Undone Project',
      finishProject: 'Finish Project',
      table: {
        index: 'Index',
        id: 'ProjectID',
        name: 'ProjectName',
        address: 'Address',
        dbName: 'DatabaseName',
        monitoringUnit: 'MonitoringUnit',
        requesterUnit: 'RequesterUnit',
        period: 'Period',
        status: 'Status',
        operation: 'Operation',
        startTime: 'StartTime',
        endTime: 'EndTime',
        securityLevel: 'SecurityLevel',
        principal: 'Principal',
        projectType: 'ProjectType',
        monitor: 'Monitor',
        supervisor: 'Supervisor',
        undone: 'Undone',
        finish: 'Finish',
        mobileStationInteval: 'mobileStationInteval'
      }
    },
    monitorList: {
      title: 'MonitorList',
      table: {
        name: 'Name',
        account: 'Account',
        pass: 'PassWord',
        position: 'Position',
        tel: 'TelePhone',
        email: 'Email',
        titleCertificate: 'TitleCertificate',
        titleCertificateID: 'TitleCertificateID',
        inductionCertificate: 'InductionCertificate',
        inductionCertificateID: 'InductionCertificateID',
        qualification: 'Qualification'
      }
    },
    supervisionList: {
      title: 'SupervisionList',
      table: {
        name: 'Name',
        account: 'Account',
        pass: 'PassWord',
        email: 'Email',
        tel: 'TelePhone',
        position: 'Position',
        unitno: 'Unitno',

      }
    },
    WarningRecord: {
      table: {
        name: 'Name',
        projectName: 'ProjectName',
        sensorType: 'SensorType',
        pointName: 'PointName',
        warningDetails: 'WarningDetails',
        time: 'Time',
        details: 'Details',
      }
    },
    Equipment: {
      table: {
        senorno: 'SenorNo',
        verificationType: 'VerificationType',
        monitorName: 'MonitorName',
        ProjectName: 'ProjectName',
        address: 'Address',
        senorName: 'SenorName',
        senorType: 'SenorType',
        lastInspectionTime: 'LastInspectionTime',
        lastInspectionMan: 'LastInspectionMan',
        producetionTime: 'ProducetionTime',
        overdue: 'Overdue',
        normal: 'Normal'
      }
    },
    // 预警参数字段
    warningKey: {
      name: 'WarningName',
      ac_u: 'Ac_u',
      ac_l: 'Ac_l',
      ac: 'Ac',
      this: 'This',
      v_u: 'V_u',
      v_l: 'V_l',
      X_ac_u: 'X_Ac_u',
      X_ac_l: 'X_Ac_l',
      Y_ac_u: 'Y_Ac_u',
      Y_ac_l: 'Y_Ac_l',
      Z_ac_u: 'Z_Ac_u',
      X_this: 'X_This',
      Y_this: 'Y_This',
      Z_this: 'Z_This',
      rap: 'Rap',
      day_rap: 'Day_Rap',
    }
  }
}

export default zh;