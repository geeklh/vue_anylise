import { TableColumn } from "element-ui";

const zh_TW = {
  // layout
  publicOperation: {
    operation: '操作',
    edit: '編輯',
    add: '添加',
    delete: '刪除',
    updata: '上傳',
    download: '下載',
    determine: '確定',
    cancel: '取消',
    save: '保存',
    details: '詳情',
    warning: '預警',
    undone: "監測中",
    finish: '已完成',
    all: "全部",
    return: "返回"
  },
  commons: {
    xiaoai: '小爱',
    superAdmin: '超級管理員',
    admin: '管理員',
    editor: '赵晓编',
    quit: '退出',
    hi: '您好',
    index: '首頁',
    userManage: '用戶管理',
    share: '數據録入',
    infoManage: '信息管理',
    // 信息管理子菜单-
    projectInformation: '項目信息',
    monitoringProgram: '監測方案',
    measuringPointManage: '測點設置',
    floorPlan: '平面圖設置',
    // -信息管理子菜单
    loindex: '主頁',
    projectManage: '項目管理',
    fileManage: '文件管理',
    // 项目详情
    projectDetails: '項目詳情',
    // 项目详情
    infoShow: '個人信息',
    infoShow1: '個人信息子菜單1',
    infoShow2: '個人信息子菜單2',
    infoShow3: '個人信息子菜單3',
    infoShow4: '個人信息子菜單4',
    infoShow5: '個人信息子菜單5',
    infoModify: '修改信息',
    infoModify1: '修改信息子菜單1',
    infoModify2: '修改信息子菜單2',
    infoModify3: '修改信息子菜單3',
    // 报表管理
    fundManage: '資金管理',
    fundList: '資金流水',
    chinaTabsList: '區域投資',
    // 数据查看
    fundData: '資金數據',
    fundPosition: '投資分佈',
    typePosition: '項目分佈',
    incomePayPosition: '收支分佈',
    curve: '曲綫',
    table: '表格',
    curveDetails: '曲綫詳情',
    // 成果管理
    resultManage: '成果管理',
    // 预警管理
    warningManage: '預警管理',
    warningProcessing: '預警處理',
    SMSManage: '短信記録',
    // equipmentManage
    equipmentManage: '儀器管理',

    permission: '權限設置',
    pagePer: '頁面權限',
    directivePer: '按鈕權限',
    errorPage: '錯誤頁面',
    page401: '401',
    page404: '404',
    wechatNumber: '微信號',
    curveGenerator: '曲綫配置',
    // 机构管理
    institution: '機構管理',
    institutionManage: '機構管理',
  },
  index: {
    yearLoss: '年度總盈虧',
    yearProfit: '年度收益率',
    potentialInvestor: '潛在投資人',
    intentionInvestor: '意向投資人',
    waitExamineInvestor: '待審投資人',
    examiningInvestor: '審覈中投資人',
    tenMillion: '千萬元',
    person: '人'
  },
  projectManage: {
    tabPane: {
      projectList: '項目列表',
      monitorList: '監測員管理',
      supervisionList: '監督員管理',
      unitInformationList: '機構信息管理',
      WarningRecord: '預警記録管理',
      Equipment: '設備管理'
    },
    unitInformationList: {
      title: '機構信息管理',
      from: {
        unitno: '單位名稱',
        address: '單位地址',
        region: '機構性質',
        cntacts: '聯繫人',
        phone: '聯繫電話',
        email: '郵箱',
        selectedOptions: '所屬區域',
        upload1: '選取資質文件',
        upload2: '選取營業執照',
        upload3: '選取稅務登記證明',
      }
    },
    projectList: {
      addProject: '新增項目',
      allProject: '全部項目',
      undoneProject: '監測中項目',
      finishProject: '已完成項目',
      table: {
        index: '序號',
        id: '項目編號',
        name: '項目名',
        address: '項目地址',
        dbName: '數據庫名',
        monitoringUnit: '監測單位',
        requesterUnit: '委託單位',
        period: '監測週期',
        status: '監測狀態',
        operation: '操作',
        startTime: '開始時間',
        endTime: '結束時間',
        securityLevel: '安全等級',
        principal: '負責人',
        projectType: '項目類型',
        monitor: '監測人員',
        supervisor: '監督人員',
        undone: '監測中',
        finish: '已完成',
        mobileStationInteval: '流動測站時間'

      }
    },
    monitorList: {
      title: '監測員列表',
      table: {
        name: '姓名',
        account: '賬號',
        pass: '密碼',
        position: '職務',
        tel: '電話',
        email: '郵箱',
        titleCertificate: '職稱證書名稱',
        titleCertificateID: '職稱證書編號',
        inductionCertificate: '上崗證書名稱',
        inductionCertificateID: '上崗證書編號',
        qualification: '資質'
      }
    },
    supervisionList: {
      title: '監督員列表',
      table: {
        name: '姓名',
        account: '賬號',
        pass: '密碼',
        tel: '電話',
        email: '郵箱',
        position: '職務',
        unitno: '歸屬機構',

      }
    },
    WarningRecord: {
      table: {
        name: '賬戶名',
        projectName: '項目名',
        sensorType: '監測類型',
        pointName: '點名',
        warningDetails: '預警詳情',
        time: '時間',
        details: '詳情',
      }
    },
    Equipment: {
      table: {
        senorno: '設備編號',
        verificationType: '檢定預警',
        monitorName: '檢定員',
        ProjectName: '當前項目',
        address: '地址',
        senorName: '設備名',
        senorType: '設備類型',
        lastInspectionTime: '最後檢定時間',
        lastInspectionMan: '檢定人',
        producetionTime: '生產時間',
        overdue: '過期',
        normal: '正常'
      }
    },
    // 预警参数字段
    warningKey: {
      name: '預警名稱',
      ac_u: '累計上限',
      ac_l: '累計下限',
      ac: '累計',
      this: '本次',
      v_u: '值上限',
      v_l: '值下限',
      X_ac_u: 'X方向累計上限',
      X_ac_l: 'X方向累計下限',
      Y_ac_u: 'Y方向累計上限',
      Y_ac_l: 'Y方向累計下限',
      Z_ac_u: 'Z方向累計上限',
      X_this: 'X方向本次',
      Y_this: 'Y方向本次',
      Z_this: 'Z方向本次',
      rap: '變化速率',
      day_rap: '日變化',
    }

  }
}

export default zh_TW;