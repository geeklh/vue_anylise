
import * as mUtils from '@/utils/mUtils'

import { getToken, setToken, removeToken } from '@/utils/auth'


const projectDetails = {
  state: {
    projectName: '',
    latLng: {},
    mapSelectionObject: {},//地图选点数据
    sensorType: ''
  },
  getters: {
    projectName: state => state.projectName,
    projectLatLng: state => state.latLng,
    mapSelectionObject: state => state.mapSelectionObject,
    sensorType: state => state.sensorType
  },
  mutations: {
    SET_PROJECTNAME: (state, name) => {
      state.projectName = name
    },
    SET_PROJECTLATLNG: (state, latLng) => {
      state.latLng = {}
      console.log("state保存：", latLng);
      state.latLng = latLng
    },
    SET_MAPSELECTIONOBJECT: (state, data) => {
      state.mapSelectionObject = data
    },
    SET_SENSORTYPE: (state, sensorType) => {
      state.sensorType = sensorType
    }

  },
  actions: {
    setProjectName: ({ commit }, name) => {
      commit('SET_PROJECTNAME', name)
    },
    setProjectLatLng: ({ commit }, latLng) => {
      commit('SET_PROJECTLATLNG', latLng)
    },
    setMapSelectionObject: ({ commit }, data) => {
      commit('SET_MAPSELECTIONOBJECT', data)
    },
    setSensorType: ({ commit }, sensorType) => {
      commit('SET_SENSORTYPE', sensorType)
    }

  }
}

export default projectDetails;
