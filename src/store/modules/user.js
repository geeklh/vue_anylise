
import * as mUtils from '@/utils/mUtils'
import { logout, getUserInfo } from '@/api/user'  // 导入用户信息相关接口
import { getToken, setToken, removeToken } from '@/utils/auth'


const user = {
  state: {
    name: '',
    avatar: '',
    unitName: '',
    token: getToken('Token'),
    roles: [],
    userData: {},
    browserHeaderTitle: mUtils.getStore('browserHeaderTitle') || 'Admin'
  },
  getters: {
    token: state => state.token,
    roles: state => state.roles,
    avatar: state => state.avatar,
    unitName: state => state.unitName,
    name: state => state.name,
    userData: state => state.userData,
    browserHeaderTitle: state => state.browserHeaderTitle,
  },
  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token || getToken('Token')
      // console.log("vuex内token", getToken('Token'));
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles
    },
    SET_BROWSERHEADERTITLE: (state, action) => {
      state.browserHeaderTitle = action.browserHeaderTitle
    },
    SET_NAME: (state, name) => {
      state.name = name
    },
    SET_AVATAR: (state, avatar) => {
      state.avatar = avatar
    },
    SET_UNITNAME: (state, unitName) => {
      state.unitName = unitName
    },
    SET_USERDATA: (state, data) => {
      state.userData = data
    }
  },
  actions: {
    //登出
    LogOut({ commit, reqData }) {
      return new Promise((resolve, reject) => {
        logout(reqData).then(response => {
          commit('SET_ROLES', [])
          removeToken('Token')
          removeToken('Role')
          resolve()
        })
      })
    },
    // 动态修改权限;本实例中,role和token是相同的;
    ChangeRoles({ commit }, role) {
      return new Promise(resolve => {
        const token = role;
        setToken("Token", token)
        getUserInfo({ "token": token }).then(res => {
          let data = res.data.userList;
          commit('SET_ROLES', data.roles)
          commit('SET_NAME', data.name)
          commit('SET_AVATAR', data.avatar)
          resolve()
        })
      })
    },
    setUserData: ({ commit }, data) => {
      commit('SET_USERDATA', data)
    },
    setTokenData: ({ commit }, token) => {
      commit('SET_TOKEN', token)
    },
  }
}

export default user;
