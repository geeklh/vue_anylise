# To start

This is a project template for [vue-cli](https://github.com/vuejs/vue-cli)

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8081
npm run dev

# build for production with minification
npm run build

```
## 用户角色
['admin', 'monitor', 'supervisor', 'supervisoryOrganization', 'editor']


### 超级管理员(superAdmin)
1.institution 模块为超级管理员管理部分，管理机构信息和人员管理（机构管理员信息）
    图片上传部分为统一公用模块，未确定上传和下载模式，暂未实现


#### 机构管理员(admin)


#### 监督机构(supervisoryOrganization)

